# palindromeCheck.py
# Programmer: Demar Brown (Demar Codes)
# Date: Jan 1, 2021
# Program Details: This program checks if a string entered by a user is a palindrome.
                    #A palindrome is a word that reads the same forwards as backwards
                    # An example is 'racecar' or 'madam'

#-------------------
# ------------------------------

def palindrome_test(user_string):

    user_string = user_string.lower()
    user_input = []
    input_reverse = []
    char_idx = 0
    chk_status = True

    for charac in user_string:
        user_input.append(charac)
        input_reverse.append(charac)

    # print(user_input)
    
    input_reverse.reverse()
    # print(input_reverse)
    
    for a in user_input:

        if a == input_reverse[char_idx]:
            print("Same letter")
        else:
            chk_status = False
            break

        char_idx= char_idx + 1

    return chk_status

#Program starts here
is_palindrome = True

user_string =  input("\nThis is a palindrome checker. Enter your string here: ")
is_palindrome = palindrome_test(user_string)

if is_palindrome is True:
    print(user_string + " is a palindrome.")
elif is_palindrome is False:
    print(user_string + " is NOT a palindrome.")